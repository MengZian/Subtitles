1
00:00:01.000 --> 00:00:05.569
Hello, this is a brief video of my MultiReplay device.

2
00:00:06.000 --> 00:00:09.139
It's kind of showing on prior servlet, and...

3
00:00:09.541 --> 00:00:11.000
Here we go, so...

4
00:00:11.500 --> 00:00:14.000
It's got this low basic menu system here.

5
00:00:14.001 --> 00:00:20.000
Set up your system, and choose different console types.

6
00:00:21.000 --> 00:00:25.000
Go and load your movie.

7
00:00:28.000 --> 00:00:32.999
Can I just see the wiring and pinouts, because it syncs for multiple consoles,

8
00:00:33.000 --> 00:00:36.500
this different wiring depending on what kind of console you are using.

9
00:00:37.000 --> 00:00:39.813
So in this case, what I will show is...

10
00:00:40.000 --> 00:00:45.000
Which connection do I get to which pins? All right, here.

11
00:00:48.000 --> 00:00:52.000
It's got an SD card over here on the side, so...

12
00:00:55.000 --> 00:00:59.000
At present we reload the configuration.

13
00:01:00.000 --> 00:01:04.000
"Setup", it's got settings in it.

14
00:01:10.000 --> 00:01:16.000
The game console setting can set different settings depending on

15
00:01:16.001 --> 00:01:18.999
the console you are actually operating.

16
00:01:19.000 --> 00:01:22.000
For now I did Nintendo and Genesis,

17
00:01:22.500 --> 00:01:26.000
and next will probably be ... Master System.

18
00:01:27.000 --> 00:01:30.801
So in this case I have got Rockman we reloaded to sync with Mega Man...

19
00:01:31.000 --> 00:01:36.000
We got to get into... "start playback". Right now I am powering from the console.

20
00:01:37.000 --> 00:01:40.000
So it has already set up to load this movie automatically.

21
00:01:40.001 --> 00:01:44.000
Someone is going ahead to turn this off and turn off the console.

22
00:01:45.000 --> 00:01:47.000
Then turn on the TV.

23
00:01:47.201 --> 00:01:53.000
And then turn on the console that should automatically start, playing this movie.

24
00:02:30.000 --> 00:02:33.000
So that's it!

25
00:02:35.000 --> 00:02:42.500
As it's playing back for Nintendo it actually show(s) what buttons are being pushed on the LCD there.

26
00:02:43.000 --> 00:02:46.800
Here there's a green LED went out when you watch when you go out the movie is done in it.

27
00:02:46.801 --> 00:02:50.710
There isn't any light frames. This white LED should actually light up, so...

28
00:02:50.711 --> 00:02:52.999
Anyway, that's the rightest success so far.

29
00:02:53.000 --> 00:02:57.000
If you are interested, let me know on IRC or email.
